import '../App.css';

import Header from './HeaderDashboard';
import React, { Suspense, lazy, useEffect, useState } from 'react';

import axios from 'axios';

import { useHistory } from 'react-router';
import {url} from '../BaseUrl';
import { FaEdit, FaTrash, FaWindowRestore } from 'react-icons/fa';

function Acceuil() {
  let history = useHistory();
  const userr = localStorage.getItem('user')
  const currentUser = JSON.parse(userr);
  console.log(currentUser)

  const [tests, setTest] = useState([])

  useEffect(() => {
    axios.get(`${url}test`).then(res => {
    
      setTest(res.data)
    })
  }
  



    , [])
    const  deleteTest=(id)=>
{
  if(window.confirm('Are you sure?'))
  {
   
  axios.delete(`${url}test/delete/`+id.id)
  window.location.reload()

   
  }
}
      
return (


    <div className="app">
       <Header />
          
        <div className="app__body">
       
        <div  >
        <div> <table>
  <tr>
  <th> question</th>
  <th>reponseA</th>
  <th>reponseB</th>
  
  <th>reponseC</th>
  <th>reponseD</th>
  <th>answer</th>
  <th>sujet</th>
  <th></th>
  </tr> 
                    {tests.map(
                    ({id, question, reponseA, reponseB, reponseC,reponseD,answer,sujet }) => {
                      return (

                        <tr>
                        <td>{question}</td>
                        <td>{reponseA}</td>
                        <td>{reponseB}</td>
                        <td>{reponseC}</td>
                        <td>{reponseD}</td>
                        <td>{answer}</td>
                        <td>{sujet}</td>

                       <td><button class="btn" onClick={()=>deleteTest({id})}><FaTrash/></button></td> 
                      
                        </tr>
 
                        
                      );

                    })}</table> </div>
                            </div>
        
      </div>
        
    </div>
);


}

export default Acceuil