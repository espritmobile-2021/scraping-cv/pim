import '../App.css';

import Header from './HeaderDashboard';
import React, { Suspense, lazy, useEffect, useState } from 'react';

import axios from 'axios';

import { useHistory } from 'react-router';
import {url} from '../BaseUrl';
import { FaEdit, FaTrash, FaWindowRestore } from 'react-icons/fa';

function Acceuil() {
  let history = useHistory();
  const userr = localStorage.getItem('user')
  const currentUser = JSON.parse(userr);
  console.log(currentUser)

  const [id, setId] = useState('');
  const [posts, setPosts] = useState([]); 
  const [state, setState] = useState({});
  const [nom, setName] = useState('');
  const [industry, setIndustry] = useState('');
  const [about, setAbout] = useState('');
  const [website, setWebsite] = useState('');
  const [type, setType] = useState('');
  const [founded, setFounded] = useState('');
  const [specialities, setSpecialities] = useState('');
  const [size, setSize] = useState('');
  const [adress, setAdress] = useState('');
  const [logo, setLogo] = useState('');

  useEffect(() => {
    axios.get(`${url}entreprise`).then((res) => {
   
        setPosts(res.data);
        setState(res.data);
    });
}, []);

const deleteCmp = (id) => {
    if (window.confirm('Are you sure?')) {
       
         axios.delete(`${url}entreprise/delete/` + id.id)
         window.location.reload()

    }
    else
    {
        
    }
};
      
      
return (


    <div className="app">
       <Header />
       <center>
       <h1>List Companies</h1>
     
       </center>
      
           
        <div className="app__body">
       
      
                            <table>
                                <tr>
                                    <th> Name</th>
                                    <th>Industry</th>
                                    <th >Type</th>
                                    <th>Company's</th>

                                    <th >logo</th>
                                    
                                    <th >Adress</th>
                                    <th>Specialities</th>
                                    <th>Website</th>
                                    <th></th>
                                </tr>
                                {posts.map(
                                    ({
                                        id,
                                        nom,
                                        industry,
                                        type,
                                        logo,
                                        adresse,
                                        specialities,
                                        website,
                                        size,
                                        about,
                                        founded
                                    }) => {
                                        return (
                                            <tr>
                                                <td >{nom}</td>
                                                <td>{industry}</td>
                                                <td>{type}</td>
                                                <td colspan="2"><img style={{borderRadius:'8px', height:'90px'}} src={`${url}images/${logo}`} /></td>

                                                <td>{adresse}</td>
                                                <td>{specialities}</td>
                                                <td>
                                                    <a
                                                        style={{ color: 'blue', fontSize: '13px' }}
                                                        href={`${website}`}
                                                    >
                                                        {website}
                                                    </a>
                                                </td>

                                                <td>
                                                    <button
                                                        class='btn'
                                                        onClick={
                                                       
                                                            () => deleteCmp({ id })
                                                      
                                                        
                                                        }
                                                    >
                                                        <FaTrash />
                                                    </button>
                                                </td>
                                                
                                               
                                            </tr>
                                        );
                                    }
                                )}
                            </table>
                        </div>
                            </div>
        
     
);


}

export default Acceuil