import '../App.css';
import Header from './HeaderDashboard';
import React, { Suspense, lazy, useEffect, useState } from 'react';

import axios from 'axios';

import { useHistory } from 'react-router';
import {url} from '../BaseUrl';
import { useForm } from 'react-hook-form';
function Acceuil() {
  let history = useHistory();
  const userr = localStorage.getItem('user')
  const currentUser = JSON.parse(userr);
  console.log(currentUser)

  
  const [logo, setLogo] = useState('');

  const {
    register,
    handleSubmit,

    formState: { errors }
} = useForm();



const onSubmit = (data) => {
    const hamma = {
        ...data,

        nom: data.name,
        adresse: data.adress,
        industry: data.industry,
        about: data.about,
        logo: String(data.logo),
        website: data.website,
        size: data.size,
        type: data.type,
        founded: data.founded,
        specialities: data.specialities
    };
    
    const files = document.getElementById("files");

    const formData = new FormData();
    formData.append("files", files.files[0]);
    formData.append("name", data.name);


fetch("http://localhost:3000/upload_files", {
    method: 'post',
    body: formData
})
    .then((res) => console.log(res) 
    )
    .catch((err) => ("Error occured", err));
    axios.post(`${url}entreprise/newEntreprise`, hamma).then(
        (response) => {
            history.push('/listcompany');
            

        },
        (error) => {
           // console.log(error);
        }
        
    );
   
};
     
      
      
return (


    <div className="app">
       <Header />
          
        <div className="app__body">
       
        <div className="divForm" >
            <center>
            <h1>add a new Company</h1>
            </center>
       
        <form onSubmit={handleSubmit(onSubmit)}>
                            <label htmlFor='name'>Name</label>
                                <input
                                    placeholder='name'
                                    type='text'
                                    {...register('name', {
                                        validate: (value) => value.length !== 0
                                    })}
                                    id="name" name="name"
                                />
                                {errors.name && (
                                    <p className='error'>* Please fill out this field</p>
                                )}
  <label for='adress'>Adress</label>
                                <input
                                    placeholder='adress'
                                    type='text'
                                    {...register('adress', {
                                        validate: (value) => value.length !== 0
                                    })}
                                />
                                {errors.adress && (
                                    <p className='error'>* Please fill out this field</p>
                                )}
<br></br>  
<label for="logo">Company's Logo </label>
<br></br>  <br></br>  
<input  type="file"{...register('logo', {
                                        validate: (value) => setLogo(value)
                                    })}  id="files" name="files" accept=".png , .jpg"/>
                                     
<br></br>  <br></br>                              
                                <label for='type'>Type</label>
                             
                                <select
                                    placeholder='Choose one...'
                                    type='text'
                                    {...register('type', {
                                        validate: (value) => value !== 'Choose one..'
                                    })}
                                >
                                    <option>Choose one..</option>
                                    <option value='Private'>Private </option>
                                    <option value='Public'>Public </option>
                                    <option value='Private  Limited'>Private Limited</option>
                                </select>
                                {errors.type && (
                                    <p className='error'>* Please select an item in the list.</p>
                                )}
                                <label for='indtustry'>Industry</label>
                                <select
                                    placeholder='Choose one...'
                                    type='text'
                                    {...register('industry', {
                                        validate: (value) => value !== 'Choose one..'
                                    })}
                                >
                                    <option>Choose one..</option>
                                    <option value='IT'>IT</option>
                                    <option value='Education '>Education </option>
                                    <option value='Food '>Food </option>
                                    <option value='Health'>Health</option>
                                    <option value='Aerospace  '>Aerospace </option>
                                    <option value='Transport '>Transport </option>
                                    <option value='Telecommunication  '>Telecommunication </option>
                                    <option value='Agriculture '>Agriculture </option>
                                    <option value='Construction  '>Construction </option>
                                    <option value='Pharmaceutical '>Pharmaceutical </option>
                                    <option value='Entertainment  '>Entertainment </option>
                                    <option value='Media'>Media</option>
                                    <option value='Energy  '>Energy </option>
                                    <option value='Manufacturing '>Manufacturing </option>
                                    <option value='Music  '>Music </option>
                                    <option value='Mining  '>Mining </option>
                                    <option value='Electronics  '>Electronics </option>
                                </select>
                                {errors.industry && (
                                    <p className='error'>* Please select an item in the list.</p>
                                )}

                                <label for='about'>About</label>
                                <input
                                    placeholder='about'
                                    type='text'
                                    {...register('about', {
                                        validate: (value) => value.length !== 0
                                    })}
                                />
                                {errors.about && (
                                    <p className='error'>* Please fill out this field</p>
                                )}
                                <label for='website'>website</label>
                                <input
                                    placeholder='https://'
                                    type='text'
                                    {...register('website', {
                                        validate: (value) => value.length !== 0
                                    })}
                                />
                                {errors.website && (
                                    <p className='error'> *Please fill out this field</p>
                                )}

                                <label for='founded'>year of foundation</label>
                                <input
                                    placeholder='2021'
                                    type='text'
                                    {...register('founded', {
                                        validate: (value) => value.length === 4
                                    })}
                                />
                                {errors.founded && <p className='error'>* must be a year</p>}

                                <label for='size'>Size</label>
                                <select
                                    placeholder='Choose one...'
                                    type='text'
                                    {...register('size', {
                                        validate: (value) => value !== 'Choose one..'
                                    })}
                                >
                                    <option>Choose one..</option>
                                    <option value='1- 5 employees'>1- 10 employees</option>
                                    <option value='10-50 employees'>10-50 employees</option>
                                    <option value='50-100 employees'>50-100 employees</option>
                                    <option value='more than 100 employees'>
                                        more than 100 employees
                                    </option>
                                </select>
                                {errors.size && (
                                    <p className='error'>* Please select an item in the list.</p>
                                )}

                                <label for='specialities'>Specialities</label>
                                <input
                                    placeholder='specialities'
                                    type='text'
                                    {...register('specialities', {
                                        validate: (value) => value.length !== 0
                                    })}
                                />
                                {errors.specialities && (
                                    <p className='error'>* Please fill out this field</p>
                                )}

                                <button
                                    type='submit'
                                    style={{
                                        width: '100%',
                                        backgroundColor: 'black',
                                        color: 'white',
                                        padding: '14px 20px',
                                        margin: '8px 0',
                                        border: 'none',
                                        cursor: 'pointer'
                                    }}
                                >
                                    Add Company
                                </button>
                            </form>
                            </div>
        
      </div>
        
    </div>
);


}

export default Acceuil