import '../App.css';
import Header from './HeaderDashboard';
import React, { Suspense, lazy, useEffect, useState } from 'react';

import axios from 'axios';

import { useHistory } from 'react-router';
import {url} from '../BaseUrl';
import { useForm } from 'react-hook-form';
function Acceuil() {
  let history = useHistory();
  const userr = localStorage.getItem('user')
  const currentUser = JSON.parse(userr);
  console.log(currentUser)

  
  const [logo, setLogo] = useState('');

  const {
    register,
    handleSubmit,

    formState: { errors }
} = useForm();



const onSubmit = (data) => {
    const hamma = {
        ...data,

        question: data.question,
        reponseA: data.reponseA,
        reponseB: data.reponseB,
        reponseC: data.reponseC,
        
        reponseD: data.reponseD,
        sujet: data.sujet,
        answer: data.answer
    };
    
    const files = document.getElementById("files");

    const formData = new FormData();
   
    formData.append("question", data.question);



    axios.post(`${url}test/newTest`, hamma).then(
        (response) => {
            history.push('/listTest');
            

        },
        (error) => {
           // console.log(error);
        }
        
    );
   
};
     
      
      
return (


    <div className="app">
       <Header />
          
        <div className="app__body">
       
       
        <div className="divForm" >
        <center>
            <h1>add a new Test</h1>
            </center>
        <form onSubmit={handleSubmit(onSubmit)}>
                            <label htmlFor='question'>Question</label>
                                <input
                                    placeholder='question'
                                    type='text'
                                    {...register('question', {
                                        validate: (value) => value.length !== 0
                                    })}
                                    id="question" name="question"
                                />
                                {errors.question && (
                                    <p className='error'>* Please fill out this field</p>
                                )}
                                 <label htmlFor='reponseA'>Question</label>
                                <input
                                    placeholder='reponseA'
                                    type='text'
                                    {...register('reponseA', {
                                        validate: (value) => value.length !== 0
                                    })}
                                    id="reponseA" name="reponseA"
                                />
                                {errors.reponseA && (
                                    <p className='error'>* Please fill out this field</p>
                                )}
                                 <label htmlFor='reponseB'>Question</label>
                                <input
                                    placeholder='reponseB'
                                    type='text'
                                    {...register('reponseB', {
                                        validate: (value) => value.length !== 0
                                    })}
                                    id="reponseB" name="reponseB"
                                />
                                {errors.reponseB && (
                                    <p className='error'>* Please fill out this field</p>
                                )}
                                 <label htmlFor='reponseC'>Question</label>
                                <input
                                    placeholder='reponseC'
                                    type='text'
                                    {...register('reponseC', {
                                        validate: (value) => value.length !== 0
                                    })}
                                    id="reponseC" name="reponseC"
                                />
                                {errors.reponseC && (
                                    <p className='error'>* Please fill out this field</p>
                                )}
                                 <label htmlFor='reponseD'>Question</label>
                                <input
                                    placeholder='reponseD'
                                    type='text'
                                    {...register('reponseD', {
                                        validate: (value) => value.length !== 0
                                    })}
                                    id="reponseD" name="reponseD"
                                />
                                {errors.reponseD && (
                                    <p className='error'>* Please fill out this field</p>
                                )}
                                 <label htmlFor='answer'>Question</label>
                                <input
                                    placeholder='answer'
                                    type='text'
                                    {...register('answer', {
                                        validate: (value) => value.length !== 0
                                    })}
                                    id="answer" name="answer"
                                />
                                {errors.answer && (
                                    <p className='error'>* Please fill out this field</p>
                                )}
                                  <label htmlFor='sujet'>Question</label>
                                <input
                                    placeholder='sujet'
                                    type='text'
                                    {...register('sujet', {
                                        validate: (value) => value.length !== 0
                                    })}
                                    id="sujet" name="sujet"
                                />
                                {errors.sujet && (
                                    <p className='sujet'>* Please fill out this field</p>
                                )}
  <button type="submit"   style={{
                                        width: '100%',
                                        backgroundColor: 'black',
                                        color: 'white',
                                        padding: '14px 20px',
                                        margin: '8px 0',
                                        border: 'none',
                                        cursor: 'pointer'
                                    }}
                                >
                                    Add Company
                                </button>
                            </form>
                            </div>
        
      </div>
        
    </div>
);


}

export default Acceuil