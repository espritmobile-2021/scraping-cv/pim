import React from 'react'
import SearchIcon from '@material-ui/icons/Search'
import '../Header.css'
import HeaderOption from '../HeaderOption'
import HomeIcon from '@material-ui/icons/Home'
import { Avatar } from '@material-ui/core'
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import BusinessCenterIcon from '@material-ui/icons/BusinessCenter'
import ChatIcon from '@material-ui/icons/Message'
import NotificationsIcon from '@material-ui/icons/Notifications';
import logo from '../assets/logoHeader.png'
import AssignmentIcon from '@material-ui/icons/Assignment';
import { useHistory } from 'react-router'
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import {url} from '../BaseUrl';
import { BsListCheck } from "react-icons/bs";
import { BsPencilSquare } from "react-icons/bs";

function Header() {
    const user = localStorage.getItem('user')
    const currentUser = JSON.parse(user);
    let history = useHistory()
    const [anchorEl, setAnchorEl] = React.useState(null);

    //open menu
    const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
    };
  
    //close menu
    const handleClose = () => {
      setAnchorEl(null);
    };
    const logoutOfApp = () => {
        localStorage.clear()
        window.location.reload()
        history.push('/login')
    }
    const navigate = (route) => {
   history.push(`${route}`)
    }
    return (
        <div className="header"   style={{backgroundColor:'black'}}>
            <div className="header__left">
                <img style={{cursor:'pointer'}} style={{backgroundColor:'white' ,width:'300px'}} src={logo} alt="" onClick={()=>navigate('/')}/>
                
               
            </div>

        
                <div className="header__right" style={{backgroundColor:'black'}}>
                <HeaderOption Icon={HomeIcon}  style={{color:'white'}} title="Home" onClick={()=>navigate('/admin')}/>
                <HeaderOption Icon={BusinessCenterIcon} title=" companies" onClick={()=>navigate('/listcompany')}/>
                <HeaderOption Icon={BsPencilSquare} title="add Company" onClick={()=>navigate('/addCompany')}/>

                <HeaderOption Icon={BsListCheck} title="tests" onClick={()=>navigate('/listTest')}/>
                
                <HeaderOption Icon={BsPencilSquare} title="add Test" onClick={()=>navigate('/addTest')}/>
                
                <MenuItem style={{fontSize:'13px',color:'gray'}} onClick={logoutOfApp} >Logout</MenuItem>

              
                 <Menu
                
                id="simple-menu"
                anchorEl={anchorEl}
               
                open={Boolean(anchorEl)}
                onClose={handleClose}
                >   
                <div onClick={()=>navigate('profile')}  style={{width:'240px',borderBottom:'1px solid lightgray'}}>
                <div style={{display:'flex',padding:'9px' ,cursor:'pointer', maxWidth:'220px'}}>
                <Avatar src={`${url}images/${currentUser.username}.jpeg`}  >{currentUser?.username[0]}</Avatar>
                <div style={{marginLeft:'10px'}}>
                    <h3 style={{lineHeight:'1.3',fontWeight:'bold'}}>{currentUser.username}</h3>  
                    <p>hamma eddine el hammewi</p>
                </div>
                </div>
                <button onClick={()=>navigate('profile')}  style={{cursor:'pointer',fontWeight:'bold',fontSize:'15px',width:'90%',color:'#eb0392',backgroundColor:'white',padding:'5px', margin:'10px' , borderRadius:'15px' ,border:'none',boxShadow:'inset 0 0 0 1px #eb0392'}}>View Profile</button>
                </div>
                <MenuItem style={{fontSize:'13px',color:'gray'}} onClick={()=>navigate('/myJobs')}  >My Jobs</MenuItem>
                <MenuItem style={{fontSize:'13px',color:'gray'}} onClick={logoutOfApp} >Settings & Privacy</MenuItem>
                <MenuItem style={{fontSize:'13px',color:'gray'}} onClick={logoutOfApp} >Languages</MenuItem>
                <MenuItem style={{fontSize:'13px',color:'gray'}} onClick={logoutOfApp} >Logout</MenuItem>
                
            </Menu>
            
            </div>
              
   </div>
    )
}

export default Header
